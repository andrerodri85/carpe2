## Scala Backend Test


To run the code: can do it either on intellij or sbt. None of the cases requires arguments. On question 5(countries), you need to start docker postgres image in order for it to find the database and populate it:

	 docker-compose up -d postgres

In order to run the tests, you will need to run "test" on sbt console. Also, you will need to to start the docker postgres image for testing on question 5 code:

	docker-compose up -d postgresTest


Some more notes:
- on some of the questions i have put responses or comments on the readme, on other cases, its on the code. 
- any question you have, feel free to email. I am also available to personally give some explanation on some of the decisions i made.

### Question 1:

In the folder `src.main.java.ngram` you can find an implementation of the Ngram class. Please explore the code and then write a comparator to compare two different n-grams based on size, count, and name.

Also, can you tell us what is the difference between a comparator and comparable?

**Response**: 

Wasn't exactly sure. My first thought was that a comparator would only work it if an object is comparable(this is, extended Comparable) but went to check it anyway: https://www.javatpoint.com/difference-between-comparable-and-comparator 

Bottom line, a Comparator is to be implemented outside an object you want to compare where Comparable should be implemented directly on the object you want to compare. With the code examples on the link its easier to understand.

First i used Ordering class from Scala, then when i read the question, searched for difference between Comparator and Comparable and find out the difference between them and a lot of methods to convert a Comparator to an Ordering or if the type in question already implements Comparable.
Also that Ordering is for Comparator as Ordered is for Comparable between Java and Scala collections....
Besides this options, Scala also allows the use of sortWith method on a collection and create an ordering method on the moment. 

### Question 2:

An instance of the Job class is used to track metadata in a long running process. A given Job instance must have an id, a type, a start date, and a description.  Ensure that a useable reference to a job object never has a state where one of these fields is null, when it is first instantiated.

Solve the problem explained above applying the standardized Builder Pattern in the `(scala/java).builder` folder.

### Question 3:

Check the following folder: `src.main.java.recipe`. Inside it you'll find an implementation of a CookBook and Recipe objects together with a main that is doing some instantiation, etc.

Can you see any problems you can see with the implementation, and if so how would you resolve them (show with additional code and comments)

### Question 4:

Solve the following problem:

Sum up a series of numbers. You must have a worker thread that reads 10 items from a queue. You must have a producer thread that populates the queue at a random interval (between 1 and 10 seconds for each number created). The producer will only produce 10 numbers. The worker thread will continue to read numbers from the queue until it has summed up all 10 numbers and returns the result. After the worker thread has completed the main program should print out the result.

**NOTE**: i didn't create unit test, would take too long to run

### Question 5 (Bonus Question):

Find a list of all countries in the world (ISO code + Country name). Create a table `countries` on the database (with proper indexing) and load the country list using your preferred database ORM lib in Java/Scala.

After the countries are loaded, read and print them in order of name length (size of the string).

### Question 6 (Super Bonus Question):

Let’s create a new backend system to use as our internal API.

**Requirements:**
- Must support long running processes(days) and the ability to cancel those processes.

- Must support high CPU utilization processes.

- Must support workflows with state dependency (do step 1 – run for a day, when complete do step 2) where each step has unique needs, 1 may require a lot of CPU power, another may need high IO and has low CPU utilization.

- Must have centralized management system.

- Centralized logging

- Authentication and authorization

**Tasks:**
- Draw the architecture
- Note the technologies used in each step
- Give examples of calls and execution workflow	

#### Implementation


**Centralized Management System(CMS)**

Responsible for task management. Has a API that allows:
- to submit a task
- retrieve a task status
- cancel task

Tech:
- Akka http for api
- Postgres Database to save tasks state
- Slick to easy integration between service and DB


**Auth-service** 

Responsible for authentication and authorization. Has an API that allows:
- user login
- token validation

Tech:
- Play framework(if we want the service to return frontend
page like login) or akka http.
- Postgres DB to store user data for login
and token validation. Slick for bridging
service and DB
- Json Web Token for validation


**Logging-service**

Responsible for handling logging events. Will populate its own database with events from Kafka. Will allows users to query for events. 

Tech:
- Akka actor + stream to read events from topic
- Akka http for API implementation
- MongoDb or other NoSql just to store events as logs(with a ttl of 1 month or so and persistence of older logs on some repository)


**Data extract service**

Service reponsible for querying external services.

Tech:
- akka http to receive submits(or an actor to receive notifications and send message to actor responsible for queueing tasks) to retrieve a task. Check architecture possibilities)
- akka streams to make requests to different services at same time hence optimizing performance. An alternative would be usage of Futures or IO solely for this or an mix with streams, enabling paralelization and containing side effects


**Data Process Service**

Data process service responsible for processing data.

Tech:
- Can be Spark if dealing with BigData, streams if it is smaller and can be processed in batches and in parallel.
- akka http or akka actor. The data passing from step 1 to step 2 don't know how to do it. Probably streams.

#### Architecture design

**1º possibility** - allow to spawn new Data extract or data processing service instances to process a new task if other instances are full. Also CMS communicates directly with Data extract or Data process services to process a task


![](doc/task_processing1.png)


Pros:
- performance - a task won't be on hold.
- Real time - CMS can be updated synchronously of changes on the task. 


Cons:
- Resource cost
- Not optimal - can happen that there is an instance with only one task. And the other instances can finish their tasks or can be cancelled. 


**2º possibility** - usage of queues to pass tasks to data extract and data process service instances, having a different queue for each kind of service. Queues be really a queue(RabbitMQ, ironMQ, amazon SQS) or it can be an actor based service to work as a gateway between services, which is a lightweight and simple solution, besides it can generate events to be sent to kafka topic and later to logging service. 


![](doc/task_processing2.png)


Pros:
- resource cost - we will only have the resources that we want.
- more chances to optimize services usage


Cons:
- Performance - tasks will be on hold


**An example/use case:** 
- want to know what kind of hazardous can happen to employees on a kitchen, alongside with usual total medical expenses for each and the probability they can be incapacitated.

- The Data extract service will have integrated the API of three different services to obtain data: 
   - one that gives statistics of how many different kinds of hazardous it happens on a kitchen and how many cases per year,up 
between 5-10 years
   - one that gives medical statistics on hospital and private sector of how many cases of each case happens and a range of the 
usual amount spent on each case
   - other gives statistics for each case where a person became incapacitated.


- The Data process service will process and group the information in the way that is requested to the CMS:
   - will calculate the probability for each hazardous case to happen, along with a range on medical expenses and probability of incapacitation. 

The flow:

Our Centralized Management System(CMS) has an API with an endpoint to submit a task.

1- The request will have fields like: place or context(kitchen), a sequence with kind of data we want to extract(possible hazardous, risks for each, medical expenses, possibility of incapacitation) and how we want it to be processed(group by hazard kind, where for each we want probability of happen, range of medical expense and probability of being incapacitated - lets assume we already defined different ways of organizing the data and the Data extract service know how to process the data as long as we specify the key and values we want on our group by).

2- CMS will verify if the request has any Json Web Token(JWT) and:
	2.1 - if it does, will send a request to auth service to validate the JWT. An invalid JWT will make the CMS to redirect the user to auth service
	2.2 - if not will redirect to login page on auth service

3- The request starts being processed: first is validated, if it has the right fields with the proper data and, if so, is transformed in to a Task type with all the required fields. The task is stored in the CMS Database.

4- After that, it depends on how the architecture is designed. Can be one of the following possibilities(with pros and cons): 
	
   - 1ºpossibility: CMS will check Data extract instances running and which one has less tasks running at the moment. It assumes a LoadBalancer role here. If all instances are maxed out, it creates a new Data extract service instance and assigns to it the new task. 
   - 2ºpossibility: There are queues between CMS and Data extract and data processing services where CMS inserts the task. The queue will notify the respective services that there is a new task on the queue.

5- Regardless of which process we choose, one of Data extract service instances is available to process it and takes it. 
NOTE: (if used the queue possibility) - Notifies CMS that it has taken the task(can be done asynchronously, with a Kafka topic for instance or synchronously, depending on if with want it to be real time or not).
	
   - The Data extract service processes the task fields to understand the nature of it and starts the proper flow. It will query the different external services to extract data. When it finishes the step, it will notify CMS(synchronously or asynchronously depending, again, on what we want and following the method mentioned before).
   - It will notify all Data processing instance that there is a task needed to be processed. the first instance that responds will receive the task to be processed, otherwise will difer to CMS. 

   - when the flow is complete on Data process service, CMS will be notified. The processed data should be made available either by the service or stored on a a second party, like a DB or something(not enough knowledge on how to handle this part).


**To monitor the status of a Task flow:**
- The user calls an endpoint on CMS which will return the status of the task. The response depends on whether the user has a proper JWT, upon validation on the auth service.
	- if it is valid: The CMS will know which instance of the Data extract service is executing, which step, etc, which is stored on CMS database.
	- if it isn't: user will be redirected to login page on auth service.
 

