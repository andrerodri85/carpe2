name := "carpe"

version := "0.1"

scalaVersion := "2.12.8"

lazy val root = ( project in file(".") )
  .settings(
    libraryDependencies ++= Seq(
      "org.scalactic" %% "scalactic" % "3.0.5",
      "org.scalatest" %% "scalatest" % "3.0.5" % "test",
      "com.typesafe.akka" %% "akka-actor" % "2.5.25",
      "com.typesafe.akka" %% "akka-testkit" % "2.5.25" % Test,
      "com.typesafe.akka" %% "akka-stream" % "2.5.25",
      "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "1.1.1",
      "com.typesafe.slick" %% "slick" % "3.3.2",
      "com.github.tminglei" %% "slick-pg" % "0.18.0",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.2",
      "org.typelevel" %% "cats-core" % "2.0.0"
    )
  )