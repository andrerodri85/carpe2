package recipe;

public class Main {
    public static void main(String[] args) {

        Recipe recipe = new Recipe("Cake");

        CookBook cookBook = new CookBook(recipe);

        /*
         * Problems:
         * 1- we don't know if CookBook type implements Cloneable type. it may return a CloneNotSupportedException
         * 2- clone() implementation on Object does what is called a shallow copy and not a deep copy, this is, it creates a new
         * instance of CookBook and assigns the same field values as the cloned instance, it doesn't copy them.
         * 3- clone() method from Cloneable returns an Object which is a supertype of CookBook. This requires a cast to
         * CookBook. There is no type safety in this. What if clone method changes CookBook nature(to a different type)?
         * Casting to CookBook will return an exception.
         * */
        CookBook newCookBook = (CookBook) cookBook.clone();

        /*
         * Mutability problem: changing the name of the recipe will change it on both
         * cookBook and newCookBook vars....
         *  I assume we want to create a new CookBook with the pie recipe.
         * */

        System.out.println("Checking cookByTheBook");
        cookBook.cookByTheBook();//will print "The Cake is a lie!"
        newCookBook.cookByTheBook();//will also print "The Cake is a lie!" because clone method on Object does a shallow
        //copy and not a deep copy...

        System.out.println("Changed recipe name to Pie");
        recipe.setName("Pie");

        cookBook.cookByTheBook();//won't print anything
        newCookBook.cookByTheBook();//same thing
    }
}