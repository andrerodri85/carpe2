package recipe

case class ScalaCookBook(private val recipe: ScalaRecipe) {

  def cookByTheBook(): Unit = {
    if (recipe.getName == "Cake") println("The Cake is a lie!")
  }

  def getRecipe: ScalaRecipe = recipe

  def setRecipe(recipe: ScalaRecipe): ScalaCookBook = {
    this.copy(recipe = recipe)
  }

  override def clone(): ScalaCookBook = {
    //new ScalaCookBook(recipe.copy()) - If we don't want ScalaCookBook to be a case class
    this.copy(recipe.copy())
  }

}
