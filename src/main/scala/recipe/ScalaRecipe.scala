package recipe

case class ScalaRecipe(private val name: String) {
  def getName: String = this.name

  def setName(newName: String): ScalaRecipe = {
    this.copy(name = newName)
  }
}
