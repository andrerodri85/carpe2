package recipe;


/*
* Too much boilerplate. Long live case classes...
*
* case class(private val name: String) {
*   def getName: String = this.name
* }
*
* */
public class Recipe {
    public Recipe(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}


