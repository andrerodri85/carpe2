package recipe

object Run extends App {

  val recipe = ScalaRecipe("Cake")

  val cookBook = new ScalaCookBook(recipe)
  val clonedCookBook = cookBook.clone()

  val recipeChangedName = recipe.setName("Pie")//new recipe instance as the fields are immutable

  //cookbook and newCookBok will still print the message even after changing the name:
  print("cookBook: ")
  cookBook.cookByTheBook()//will print
  print("clonedCookBook: ")
  clonedCookBook.cookByTheBook()//will print

  //now we change the old cookbook to the new recipe:
  val updatedCookBook = cookBook.setRecipe(recipeChangedName)//new instance of ScalaCookBook since fields are immutable
  println()
  println("After updating the cookbook with a new recipe...")
  println()
  print("cookBook: ")
  cookBook.cookByTheBook()//will print
  print("clonedCookBook: ")
  clonedCookBook.cookByTheBook()//will print
  print("updatedCookBook: ")
  updatedCookBook.cookByTheBook()//will not print

  /*
  * In summary:
  *  1- used immutability so i know what to expect on each case. If i change a recipe name i am sure i won't
  *  affect any CookBook's that were created unless i want to update them with the new recipe. If i want to change a
  *  recipe name, i have a method to do that and it also will return me a new ScalaCookBook so that any code using the
  *  non-updated ScalaCookBook won't be affected unless i want it so. We have code that easier to reason because we know
  *  what to expect
  *  2- improved clone method: not using cloneable but the case class copy method.
  * */


}
