package recipe;

/*
* Cloneable type is just a marker, it doesn't have any clone method. The clone method is present on Object type. If CookBook
* class didn't implement Cloneable, clone method from Object would return a CloneNotSupportedException.
* */
public class CookBook implements Cloneable {
    public CookBook(Recipe recipe) {
        this.recipe = recipe;
    }

    public void cookByTheBook() {
        if (recipe.getName().equals("Cake")) {
            System.out.println("The Cake is a lie!");
        }
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    /*
    * Problems:
    * 1- super.clone() refers to a method on Object not on Cloneable. If CookBook didn't implement Cloneable, it would
    * return CloneNotSupportedException. Since it implements the Cloneable, that try catch is pretty useless unless to
    * make sure that if Cloneable interface is removed from CookBook...not the best code.
    * 2- clone() implementation on Object does what is called a shallow copy and not a deep copy, this is, it creates a new
    * instance of CookBook and assigns the same field values as the cloned instance, it doesn't copy them.
    * 3- clone() method from Cloneable returns an Object which is a supertype of CookBook. This requires a cast to
    * CookBook. There is no type safety in this. What if clone method changes CookBook nature(to a different type)?
    * Casting to CookBook will return an exception.
    * 4- returning null is dangerous and any verification adds boilerplate hence more code.
    * */
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    /*
     * recipe field is mutable. Any change on recipe object will change also here, as it happens on Main:
     * when recipe name is changed from "cake" to "pie". cookByBook method will print something at first but it won't
     * print after name change to Pie. And all without using the setRecipe method, only because Recipe memory reference
     * is shared and changeable.
     * */
    private Recipe recipe;
}
