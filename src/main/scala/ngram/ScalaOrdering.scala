package ngram

object ScalaOrdering extends Ordering[Ngram] {

  //write a comparator to compare two different n-grams based on size, count, and name.
  def compare(a: Ngram, b: Ngram): Int = {
    val sizeComparison = sizeCompare(a, b)
    lazy val countComparison = countCompare(a, b)
    lazy val nameComparison = nameCompare(a, b)
    if(sizeComparison == 0) {
      if (countComparison == 0){
        nameComparison
      } else countComparison
    } else sizeComparison
  }

  //naming helps when defining compare method above
  private def sizeCompare(a: Ngram, b: Ngram): Int = {
    Option(a.getSize).getOrElse(0) compare Option(b.getSize).getOrElse(0)
  }

  private def countCompare(a: Ngram, b: Ngram): Int = {
    Option(a.getCount).getOrElse(0) compare Option(b.getCount).getOrElse(0)
  }

  private def nameCompare(a: Ngram, b: Ngram): Int = {
    println(s"${a.getName} and ${b.getName}")
    val p = Option(a.getName).getOrElse("") compare Option(b.getName).getOrElse("")
    println(s"$p")
    p
  }

}
