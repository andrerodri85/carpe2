package queueWorker

import akka.actor.{Actor, ActorLogging}
import queueWorker.ConsumerActor.{ReturnTotal, WhatIsTotal}
import queueWorker.QueueActor.{NextElement, NotifyPublisher, ReceivedAllElements, RequestNextNumber}

class ConsumerActor(
  var numsGenerated: Int = 0,
  var total: Int = 0
) extends Actor with ActorLogging {

  val ordinalNumber = List("first", "second", "third", "fourth", "fifth",
    "sixth", "seventh", "eighth", "ninth", "tenth")

  override def receive: Receive = {
    case NotifyPublisher =>
      sender() ! RequestNextNumber
    case NextElement(number) =>
      numsGenerated +=1
      total +=number
      println(s"ConsumerActor: received the ${ordinalNumber(numsGenerated-1)} number: $number.")
      if(numsGenerated == 10){
        println(s"Total is: $total")
        sender() ! ReceivedAllElements
      }
    case WhatIsTotal =>
      sender() ! ReturnTotal(total)
      context.stop(self)
      System.exit(0)
  }
}

object ConsumerActor {

  case object WhatIsTotal

  case class ReturnTotal(total: Int)
}