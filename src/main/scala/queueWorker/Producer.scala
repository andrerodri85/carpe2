package queueWorker

import akka.actor.ActorRef
import queueWorker.Producer.AddNumber
import scala.util.Random

class Producer(
  queue: ActorRef,
  var numsGenerated: Int = 0
) {

  val ordinalNumber = List("first", "second", "third", "fourth", "fifth",
    "sixth", "seventh", "eighth", "ninth", "tenth")

  def start = {

    /*
    * Using mutability and while loop: not the most graceful solution...
    * */
    while (numsGenerated < 10) {
      /*
      * Scala Random start at 0 and we want between 1 and 10 seconds. in nextInt, the arg is not inclusive but we are
      * adding 1 always so if 9, which is the max, we add 1 to give us 10
      * */
      var waitingTime = 1 + Random.nextInt(1)
      println(s"Producer is waiting $waitingTime second(s)")
      Thread.sleep(waitingTime * 1000)

      val number = Random.nextInt(21)//21 not inclusive
      numsGenerated += 1
      println(s"ProducerActor: produced the ${ordinalNumber(numsGenerated - 1)} number: $number ")
      queue ! AddNumber(number)

    }
    println("Producer: shutting down")
  }
}

object Producer {

  case class AddNumber(number: Int)

}
