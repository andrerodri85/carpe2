package queueWorker

import akka.actor.{ActorSystem, Props}

object QueueProblemRun extends App {
  implicit val system = ActorSystem()
  implicit val ec = system.dispatcher


  //creating consumer first
  val consumerActor = system.actorOf(Props(new ConsumerActor()), name = "Consumer")

  val queue = system.actorOf(Props(new QueueActor(consumerActor)), name = "Queue")

  val producer = new Producer(queue)

  producer.start

}
