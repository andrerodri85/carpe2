package queueWorker

import akka.actor.{Actor, ActorLogging, ActorRef}
import queueWorker.Producer.AddNumber
import queueWorker.QueueActor.{NextElement, NotifyPublisher, ReceivedAllElements, RequestNextNumber}

import scala.collection.mutable.Queue

/*
* Its a bit more complicated to be implemented for multiple publishers since the dequeue for each publisher
* happens at a different time and we can't dequeue permanently until all publishers receive the element. Probably won't
* use dequeue but an offset or an index on the RequestNextNumber, like its used with kafka topics. The next problematic
 * it brings is that it cannot be an infinite queue, so a time interval would needed to be set on the queue to retain the
 * elements. Example: a queue mantains a number for 24 hours. After that, its discarded.
* */
class QueueActor(
  publisher: ActorRef,
  queue: Queue[Int] = new Queue()
) extends Actor with ActorLogging {

  override def receive: Receive = {
    case AddNumber(number) =>
      println(s"QueueActor: enqueueing number $number")
      queue.enqueue(number)
      publisher ! NotifyPublisher
    case RequestNextNumber =>
      val number = queue.dequeue()
      println(s"QueueActor: sending number $number to publisher.")
      sender() ! NextElement(number)
    case ReceivedAllElements =>
      //println("QueueActor is shutting down.")
      context.stop(self)
  }
}

object QueueActor {

  case object NotifyPublisher

  case object RequestNextNumber

  case class NextElement(number: Int)

  case object ReceivedAllElements

}