package builder

import java.time.LocalDate

sealed trait BuildStep
sealed trait HasId extends BuildStep
sealed trait HasType extends BuildStep
sealed trait HasStartDate extends BuildStep
sealed trait HasDescription extends BuildStep

/*
* setter methods must be called by field order so that a ScalaJob can be created.
* This solution has some problems imho:
* 1 - complexity(the more fields, the more complex it gets)
* 2 - uses mutability
* 3 - requires that the setters be called in a certain order so it can build
* */
class ScalaJobBuilder[PassedStep <: BuildStep] private(
  var id: Int,
  var `type`: String, //Should be Enum or an ADT if possible types are know
  var startDate: LocalDate,
  var description: String
) {

  protected def this() = this(0, "", LocalDate.now(), "")

  protected def this(b: ScalaJobBuilder[_]) = this(
    b.id,
    b.`type`,
    b.startDate,
    b.description
  )

  def setId(id: Int): ScalaJobBuilder[HasId] = {
    this.id = id
    new ScalaJobBuilder[HasId](this)
  }

  def setType(`type`: String)(implicit ev: PassedStep =:= HasId)
  : ScalaJobBuilder[HasType] = {
    assert(`type` != null)
    this.`type` = `type`
    new ScalaJobBuilder[HasType](this)
  }

  def setStartDate(startDate: LocalDate)(implicit ev: PassedStep =:= HasType)
  : ScalaJobBuilder[HasStartDate] = {
    assert(startDate != null)
    this.startDate = startDate
    new ScalaJobBuilder[HasStartDate](this)
  }

  def setDescription(description: String)(implicit ev: PassedStep =:= HasStartDate)
  : ScalaJobBuilder[HasDescription] = {
    assert(description != null)
    this.description = description
    new ScalaJobBuilder[HasDescription](this)
  }

  //Will only build when setDescription was called
  def build()(implicit ev: PassedStep =:= HasDescription): ScalaJob = ScalaJob(
    id,
    `type`,
    startDate,
    description
  )
}

object ScalaJobBuilder {
  def apply() = new ScalaJobBuilder[BuildStep]()
}
