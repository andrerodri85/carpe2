package builder

import java.time.LocalDate

case class ScalaJob(
  id: Int,
  `type`: String, //Should be Enum or an ADT if possible types are know
  startDate: LocalDate,
  description: String
)
