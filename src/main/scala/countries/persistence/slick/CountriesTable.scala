package countries.persistence.slick

import countries.model.Country
import slick.lifted.{TableQuery, Tag}
import slick.jdbc.PostgresProfile.api._

/*
* slick representation of countries table
* */
class CountriesTable(tag: Tag) extends Table[Country](tag,"countries"){

  val isoCode = column[String]("iso_code", O.PrimaryKey)
  val name = column[String]("name")


  def * = (isoCode, name).mapTo[Country]

  def nameIndex = index("country_name", name, unique=true)
}

object CountriesQ extends TableQuery(new CountriesTable(_)) {
  def countriesOrderByNameSize = {
    this
      .sortBy(_.name)
  }

  def createTable = {

  }

  /*
  * Method used to insert a batch. Stream to load table calls this method
  * */
  def insertBatch(batch: Seq[Country]) = {
    this ++= batch
  }

  /*
  * Truncate method, useful for when testing occurs
  *
  * */
  def truncate = {
    this.delete
  }
}
