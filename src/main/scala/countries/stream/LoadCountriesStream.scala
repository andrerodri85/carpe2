package countries.stream

import akka.stream.alpakka.csv.scaladsl.CsvParsing
import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink}
import akka.stream.{ActorMaterializer, Attributes}
import akka.util.ByteString
import countries.model.Country
import countries.persistence.slick.PostgresProfile.api.Database
import cats.implicits._

import java.nio.file.Paths
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try
import FlowOps._
import countries.persistence.slick.CountriesQ

class LoadCountriesStream(
  filename: String,
  db: Database
)(implicit val ec: ActorMaterializer, implicit val ex: ExecutionContext) {

  /*
  * Allows to create a sink to deal with Left cases on divertLeft
  * */
  def writeFailedCases[R](filename: String) =
    Flow[Either[ByteString, R]]
      .collect {
        case Left(data) => data
      } //return a collection of ByteString
      .toMat(FileIO.toPath(Paths.get(filename)))(Keep.left)

  private def convert(row: List[ByteString]) = {
    Try {
      Country(
        name = row(0).utf8String,
        isoCode = row(1).utf8String
      )
    }
  }

  /*
  * selecting only the columns we are interested. If they are found and parsed correctly,
  * we have Future of Either Right. Othwerwise, we have Either Left. This separation will be useful
  * to write failed cases on a file.
  * */

  val csvParsing =
    CsvParsing
      .lineScanner()
      .mapAsync(100) { row =>
        val conversion = convert(row)
          .toEither
          .leftMap { ex =>
            ByteString(s"Unable to parse row. Reason: ${ex.getMessage}. Row: $row.\n")
          } //cats library provides some new features, reducing boilerplate
        Future.successful(conversion)
      }
      .divertLeft(writeFailedCases[Country]("FailedParsedRow.txt"))//row failed to convert by some reason will go to this file

  /*
  *
  * Flow that deals with DB insertion on 'countries' table. It groups a batch of 50 countries elements and it has next
  * a mapAsync of 10 workers. So in theory it could be inserting 100 elements at the same time(50*2). In practise, the
  * batch insertions on the DB may not occur at the same time. Plus, initially, some workers will have to wait until
  * enough elements of 50 grouped in order for work to be passed to them.
  * */
  val insertOnTable =
    Flow[Country]
    .grouped(50)
    .mapAsync(2) { dataSeq =>
      db.run(CountriesQ.insertBatch(dataSeq))
        .map(_ => Right(dataSeq))
        .recover { case ex =>
            Left(ByteString(s"Insert of batch ${dataSeq} on table failed."))
        }
    }
    .divertLeft(writeFailedCases("FailedbatchInsertion.txt"))//failed insertion operations will be written in this file

  val stream =
    FileIO.fromPath(Paths.get(filename))
    .via(csvParsing)
    .via(insertOnTable)
    .log(name = "LoadCountriesStream")
    .addAttributes(Attributes.logLevels(
      onElement = Attributes.LogLevels.Off,
      onFailure = Attributes.LogLevels.Error,
      onFinish = Attributes.LogLevels.Info))
    .toMat(Sink.ignore)(Keep.right)

  def run = stream.run()
}
