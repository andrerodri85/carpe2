package countries

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import persistence.slick.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration._

object CountriesOrdering extends App {

  implicit val system = ActorSystem("system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val db = Database.forConfig("db.main")

  val loader = new LoadCountries(db)

  val result = for {
    _ <- loader.populate
    query <- loader.sortByBiggerName
  } yield query

  val countries = Await.result(result, 3 seconds)

  countries.foreach(println)

  db.close()
}
