package countries

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import countries.persistence.slick.CountriesQ
import countries.stream.LoadCountriesStream
import persistence.slick.PostgresProfile.api._

import scala.concurrent.ExecutionContext

class LoadCountries(
  db: Database
)(implicit val ec: ExecutionContext, implicit val materializer: ActorMaterializer) {

  val stream = new LoadCountriesStream("countries.csv", db)

  private def createDB = CountriesQ.schema.dropIfExists andThen CountriesQ.schema.create

  def populate = {
    for {
      _ <- db.run(createDB)
      _ <- stream.run
        .map { _ =>
          println(s"Loadscript completed.")
        }
    } yield ()
  }

  private def getCountriesList = db.run(CountriesQ.result)

  /*
  * Ordering from bigger name to smaller name - DESC
  * */
  def sortByBiggerName = {
    getCountriesList
      .map(_.sortWith(_.name.length > _.name.length))

  }

}
