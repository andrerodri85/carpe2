package countries.model

case class Country(
  isoCode: String,
  name: String
) {

  override def toString(): String = s"$name;$isoCode"

}
