package countries

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import countries.model.Country
import countries.persistence.slick.CountriesQ
import org.scalatest.{BeforeAndAfter, FunSuite}
import persistence.slick.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration._

class CountriesOrderingTest extends FunSuite with BeforeAndAfter {

  val db = Database.forConfig("db.test")
  implicit val system = ActorSystem("system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  val loader = new LoadCountries(db)

  before {
    val populateRequest = loader.populate
    //Await asserts that loading the database shouldn't take more than 3 seconds
    Await.result(populateRequest, 3 seconds)
  }

  test("Should return list of countries ordered by biggest name") {
    val countries = Await.result(loader.sortByBiggerName, 3 seconds)
    assertResult(countries.headOption) {
      Some(Country("GB", "United Kingdom of Great Britain and Northern Ireland"))
    }
  }

  after {
    db.close()
  }
}
