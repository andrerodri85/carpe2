package ngram

import org.scalatest.FunSuite


class NgramOrderingTest extends FunSuite {
  test("First Ngram with lower size than second Ngram should be lower") {
    val first = new Ngram()
    first.setSize(9)
    val second = new Ngram()
    second.setSize(10)
    assertResult(ScalaOrdering.compare(first, second)) {
      -1
    }
  }

  test("two Ngrams with no fields defined should be equal") {

  }

  test("First Ngram with same size and no more fields define should be equal") {
    val first = new Ngram()
    first.setSize(10)
    val second = new Ngram()
    second.setSize(10)
    assertResult(ScalaOrdering.compare(first, second)) {
      0
    }
  }

  test("First Ngram with same size and different count should be different") {
    val first = new Ngram()
    first.setSize(10)
    first.setCount(1)
    val second = new Ngram()
    second.setSize(10)
    second.setCount(2)
    assert(ScalaOrdering.compare(first, second) != 0)
  }

  test("First Ngram with same size and count but no name should be equal") {
    val first = new Ngram()
    first.setSize(10)
    first.setCount(1)
    val second = new Ngram()
    second.setSize(10)
    second.setCount(1)
    assert(ScalaOrdering.compare(first, second) == 0)
  }

  test("First Ngram with same size, count but different name should be different") {
    val first = new Ngram()
    first.setSize(10)
    first.setCount(1)
    first.setName("black")
    val second = new Ngram()
    second.setSize(10)
    second.setCount(1)
    second.setName("white")
    assert(ScalaOrdering.compare(first, second) != 0)
  }

  test("First Ngram with same size, count and name should be equal") {
    val first = new Ngram()
    first.setSize(10)
    first.setCount(1)
    first.setName("equal")
    val second = new Ngram()
    second.setSize(10)
    second.setCount(1)
    second.setName("equal")
    assert(ScalaOrdering.compare(first, second) == 0)
  }
}
