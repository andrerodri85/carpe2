package builder

import org.scalatest.FunSuite

import java.time.LocalDate

class ScalaJobBuilderTest extends FunSuite {

  test("Shouldn't compile when one of the fields is not defined.") {
    assertDoesNotCompile(
      """
      ScalaJobBuilder
      .apply()
      .setId(1)
      .setType("Admin")
      .setStartDate(LocalDate.now())
      .build()"""
    )
  }

  test("Shouldn't allow to create a job when type field is null") {
    assertThrows[AssertionError] {
      ScalaJobBuilder
        .apply()
        .setId(1)
        .setType(null)
        .setStartDate(LocalDate.now())
        .setDescription("")
        .build()
    }
  }

  test("Shouldn't allow to create a job when startDate field is null") {
    assertThrows[AssertionError] {
      ScalaJobBuilder
        .apply()
        .setId(1)
        .setType("Admin")
        .setStartDate(null)
        .setDescription("")
        .build()
    }
  }

  test("Shouldn't allow to create a job when description field is null") {
    assertThrows[AssertionError] {
      ScalaJobBuilder
        .apply()
        .setId(1)
        .setType("Admin")
        .setStartDate(LocalDate.now())
        .setDescription(null)
        .build()
    }
  }

  test("Should allow to create a job when all fields are defined and not null") {

    assertCompiles {
      """ScalaJobBuilder
        .apply()
        .setId(1)
        .setType("Admin")
        .setStartDate(LocalDate.now())
        .setDescription("Testing")
        .build()"""
    }

    val result = ScalaJobBuilder
      .apply()
      .setId(1)
      .setType("Admin")
      .setStartDate(LocalDate.now())
      .setDescription("Testing")
      .build()

    assertResult(result) {
      ScalaJob(1, "Admin", LocalDate.now(), "Testing")
    }
  }
}
